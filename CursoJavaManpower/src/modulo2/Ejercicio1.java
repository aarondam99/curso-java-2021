package modulo2;

public class Ejercicio1 {

	public static void main(String[] args) {

		byte bMin = 127;
		//byte----short----int----float----double(autocas)
		//int----short(necesita casteo)
		short s = bMin;
		int i = 3000;
		//necesita casteo
		float n1 = 0;
		int n2 = 3;
		float f = n1/0;
		double raiz = Math.sqrt(-1);
		byte b = (byte)i;
		int nCualquiera = 3;
		
		System.out.println("N�mero cualquiera =" + ++nCualquiera);
		System.out.println("raiz=" + raiz);
		System.out.println("float=" + f);
		System.out.println("short=" + s);
		System.out.println("bMin=" + bMin);
		System.out.println("i=" + i);
		System.out.println("b=" + b);
	}

}
