
package objetos;

import objetos.exceptions.CuentaException;

/**
 * @author Gabriel
 * Esta clase guarda los datos de una cuenta bancaria
 *
 */
/**
 * @author Maria
 *
 */
/**
 * @author Gabriel
 * Esta Clase guarda los datos de una cuenta bancaria
 *
 */
public abstract class Cuenta {
	//atributos encapsulamiento
	private static int cantidadDeCuentas;
	private int 	numero	;
	private float 	saldo	;
	
	//constructores
	public Cuenta(){
		numero = 10;
		saldo = 1000;
		cantidadDeCuentas++;
	}
	
	public Cuenta(int pNumero, float pSaldo){
		numero = pNumero;
		saldo  = pSaldo;
		cantidadDeCuentas++;
	}
	
	//accesoors publco
	
	/**
	 * Este mtodo asigna el numero de cuenta
	 * @param pNumero es el parametro con el numero de cuenta
	 */
	public void setNumero(int pNumero){		numero = pNumero;	}	
	public int getNumero(){					return numero;		}
	
	public float getSaldo() {				return saldo;		}
	public void setSaldo(float saldo) {		this.saldo = saldo;	}
	
	

	public static int getCantidadDeCuentas() {						return cantidadDeCuentas					;	}
	public static void setCantidadDeCuentas(int cantidadDeCuentas) {Cuenta.cantidadDeCuentas = cantidadDeCuentas;	}
	
	//metodos de negocio
	public void acreditar(float pMonto){
		//saldo = saldo + pMonto;
		saldo += pMonto;		
	}
	public abstract void debitar(float pMonto) throws CuentaException;

	public boolean equals(Object obj){
		//establezco las reglas de negocio como yo quiero 
		
		boolean bln =false;
		if(obj instanceof Cuenta){
			//downcast			
			Cuenta cue = (Cuenta) obj;
			bln = 	this.numero == cue.getNumero() &&
					this.saldo 	== cue.getSaldo();
		}
		return bln;
	}
	public int hashCode(){
		return numero + (int)saldo;
	}
	public String toString(){
		return "\nnumero=" + numero + ", saldo=" + saldo;
	}
}
