package objetos.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import objetos.CajaDeAhorro;
import objetos.Cuenta;
import objetos.CuentaCorriente;
import objetos.exceptions.CuentaException;

public class CuentaTest {
    Cuenta cajaDeAhorraVacio;
    Cuenta cajaDeAhorroLleno;
    Cuenta cc1Vacio;
    //lisy y set son interfaces
    List<Cuenta> lstCuentas; //pemite duplicados y es ordered (uno atras del otro
    Set<Cuenta> setCuentas; //NO PERMITE suplicado y se guardan solo DIOS sabe;

	@Before
	public void setUp() throws Exception {
		//upcast 
			
		cajaDeAhorraVacio = new CajaDeAhorro();
		cajaDeAhorroLleno = new CajaDeAhorro(20,2000,0.2f);
		cc1Vacio = new CuentaCorriente();
		//el objeto se crea con la instruccion new....
				
		lstCuentas = new ArrayList<Cuenta>();
		lstCuentas.add(new CajaDeAhorro());
		lstCuentas.add(new CuentaCorriente());
		
		lstCuentas.add(new CajaDeAhorro(1,10,0.1f));
		lstCuentas.add(new CajaDeAhorro(2,20,0.2f));
		lstCuentas.add(new CajaDeAhorro(3,30,0.3f));
		
		lstCuentas.add(new CuentaCorriente(10, 1000, 100));
		lstCuentas.add(new CuentaCorriente(20, 2000, 200));
		lstCuentas.add(new CuentaCorriente(30, 3000, 300));

		setCuentas = new HashSet<Cuenta>();
		setCuentas.add(new CajaDeAhorro());
		setCuentas.add(new CuentaCorriente());
		
		setCuentas.add(new CajaDeAhorro(1,10,0.1f));
		setCuentas.add(new CajaDeAhorro(2,20,0.2f));
		setCuentas.add(new CajaDeAhorro(3,30,0.3f));
		
		setCuentas.add(new CuentaCorriente(10, 1000, 100));
		setCuentas.add(new CuentaCorriente(20, 2000, 200));
		setCuentas.add(new CuentaCorriente(30, 3000, 300));
		//se crean 19 cuentas
		
	}

	@After
	public void tearDown() throws Exception {
		cajaDeAhorraVacio = null;
		cajaDeAhorroLleno = null;
		cc1Vacio	= null;
		lstCuentas  = null;
		setCuentas  = null;
		Cuenta.setCantidadDeCuentas(0);
		
	}
	@Test
	public void testCantidadDeCuentas(){
		assertEquals(19, Cuenta.getCantidadDeCuentas());
	}
	
	@Test
	public void testListaEqualsContainsTRUE(){
		Cuenta cPrueba = new CajaDeAhorro();
		assertTrue(lstCuentas.contains(cPrueba));
	}
	@Test
	public void testListaEqualsContainsFALSE(){
		Cuenta cPrueba = new CajaDeAhorro();
		cPrueba.acreditar(100);
		assertFalse(lstCuentas.contains(cPrueba));
	}

	@Test
	public void testListaAdd(){		
		Cuenta cPrueba = new CajaDeAhorro();		
		assertTrue(lstCuentas.add(cPrueba));
		System.out.println("lstCuentas=" + lstCuentas);
	}
	@Test
	public void testSetAdd(){
		Cuenta cPrueba = new CajaDeAhorro();		
		assertFalse(setCuentas.add(cPrueba));
		System.out.println("setCuentas=" + setCuentas);
	}
	
	
	@Test
	public void testCajaDeAhorroEqualsVERDADERO(){
		Cuenta cajaDeahorro = new CajaDeAhorro();
		assertTrue(cajaDeAhorraVacio.equals(cajaDeahorro));
	}
	
	@Test
	public void testCajaDeAhorroEqualsFALSO(){
		Cuenta cajaDeahorro = new CajaDeAhorro();
		cajaDeahorro.acreditar(100);
		assertFalse(cajaDeAhorraVacio.equals(cajaDeahorro));
	}
	
	@Test
	public void testCuenta() {
		//pruebo el contrtructor vacio
		assertEquals(10, cajaDeAhorraVacio.getNumero());
		assertEquals(1000.0f, cajaDeAhorraVacio.getSaldo(), 0.01);
		
	}

	@Test
	public void testCuentaIntFloat() {
		assertEquals(20, cajaDeAhorroLleno.getNumero());
		assertEquals(2000.0f, cajaDeAhorroLleno.getSaldo(), 0.01);
	}

	@Test
	public void testAcreditar() {
		cajaDeAhorraVacio.acreditar(100.53f);
		assertEquals(1100.53f, cajaDeAhorraVacio.getSaldo(), 0.01);
	}

	@Test
	public void testDebitarCajaDeAhorraALCANZA() {
		try {
			cajaDeAhorraVacio.debitar(100);
			assertEquals(900.0f, cajaDeAhorraVacio.getSaldo(),0.01);
		} catch (CuentaException e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}
	@Test
	public void testDebitarCajaDeAhorra_NO_ALCANZA() {
		try {
			cajaDeAhorraVacio.debitar(1100);
			assertTrue(false);
		} catch (CuentaException e) {
			assertEquals("no te alcanza que queres hacer....???", e.getMessage());
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void testDebitarCuentaCorrienteALCANZA(){
		//tengo saldo de 1000
		//descubierto 150
		try {
			cc1Vacio.debitar(200);
			assertEquals(800.0f, cc1Vacio.getSaldo(), 0.01);
		} catch (CuentaException e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}
	@Test
	public void testDebitarCuentaCorriente_ALCANZA_CON_DESCUBIERTO(){
		//tengo saldo de 1000
		//descubierto 150
		try {
			cc1Vacio.debitar(1100);
			assertEquals(-100.0f, cc1Vacio.getSaldo(), 0.01);
		} catch (CuentaException e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void testDebitarCuentaCorriente_NO_ALCANZA(){
		//tengo saldo de 1000
		//descubierto 150
		try {
			cc1Vacio.debitar(1200);
			assertTrue(false);
		} catch (CuentaException e) {
			assertEquals("se acabo...!!:(", e.getMessage());
			e.printStackTrace();
		}
		
	}	
	//TODO a los alumnos osados que agreguen los test que crean neceario hasta que pinche

}
