package objetos;

import javax.swing.SingleSelectionModel;

import objetos.exceptions.CuentaException;

public class CuentaTest {

	public static void main(String[] args) throws CuentaException {
		//crea un objeto
		Cuenta c1 = new CajaDeAhorro();
		Cuenta c2 = new CajaDeAhorro(20,2000,0.2f);
		
		System.out.println("c1=" + c1);
		System.out.println("c2=" + c2);
		//probando que el contructor funciona
		//c1 = null;
		System.out.println("se creo el objeto 1=" + c1.getNumero());
		System.out.println("su saldo es " + c1.getSaldo());
	
		System.out.println("\nse creo el objeto 2=" + c2.getNumero());
		System.out.println("su saldo es " + c2.getSaldo());
		//probar que el acreditar funciona}
		System.out.println("\na c1 le voy a acreditar 100");
		c1.acreditar(100);
		System.out.println("el nuevo saldo de c1 es " + c1.getSaldo());
		//probar que el debitar funciona
		System.out.println("\n c1 le voy a debitar 80");
		c1.debitar(80);
		System.out.println("el nuevo saldo de c1 es " + c1.getSaldo());
		
		
	}

}