package objetos;

import objetos.exceptions.CuentaException;

public class CuentaCorriente extends Cuenta {
	private float descubierto;
	
	public CuentaCorriente() {
		super(10, 1000);
		descubierto = 150;
	}
	public CuentaCorriente(int pNumero, float pSaldo, float pDescubierto) {
		super(pNumero, pSaldo);		
		descubierto = pDescubierto;
	}

	
	public float getDescubierto() {
		return descubierto;
	}

	public void setDescubierto(float descubierto) {
		this.descubierto = descubierto;
	}

	@Override
	public void debitar(float pMonto) throws CuentaException {
		if(pMonto < (getSaldo() + descubierto))
			setSaldo(getSaldo()-pMonto);
		else
			throw new CuentaException("se acabo...!!:(");
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Float.floatToIntBits(descubierto);
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof CuentaCorriente))
			return false;
		CuentaCorriente other = (CuentaCorriente) obj;
		if (Float.floatToIntBits(descubierto) != Float.floatToIntBits(other.descubierto))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "\nCuentaCorriente " + super.toString() + ",descubierto=" + descubierto ;
	}
	

}
