package objetos;

import objetos.exceptions.CuentaException;

public class CajaDeAhorro extends Cuenta {
	//atributos
	private float interes;
	
	//constructores
	public CajaDeAhorro() {		
		super(10, 1000);
		interes = 0.1f;
	}

	public CajaDeAhorro(int pNumero, float pSaldo, float pInteres) {
		super(pNumero, pSaldo);
		interes=pInteres;		
	}

	//accessors
	public float getInteres() {					return interes;			}
	public void setInteres(float interes) {		this.interes = interes;	}
	
	public void debitar(float pMonto) throws CuentaException{
		if(pMonto<=getSaldo())
			setSaldo(getSaldo()-pMonto);
		else
			throw new CuentaException("no te alcanza que queres hacer....???");
	}
	public boolean equals(Object obj){		
		return obj instanceof CajaDeAhorro 					&&
				super.equals(obj)  							&&
				interes == ((CajaDeAhorro)obj).getInteres();		
	}
	public int hashCode(){
		return super.hashCode() + (int)(interes*100);
	}
	public String toString(){
		return "\ncajaDeahorro " + super.toString() + ",interes=" + interes;
	}

}
