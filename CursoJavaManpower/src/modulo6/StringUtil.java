package modulo6;

public class StringUtil {

	public static boolean containsDobleSpace(String str) {
		
		int cont = 0;
		boolean dosEspacios = false;
		
		for (int i = 0; i < str.length() && dosEspacios == false; i++) {
			
			if(str.charAt(i) == ' ') {
				
				cont++;
			}
			
			if(cont == 2) {
				
				dosEspacios = true;
			}
		}
		
		return dosEspacios;
	}
	
	public static boolean containsNumber(String str) {
		
		String cadena = "0123456789";
		boolean tieneNumeros = false;
		
		for (int i = 0; i < str.length() && tieneNumeros == false; i++) {
			
			if(cadena.contains(str.charAt(i) + "")) {
				
				tieneNumeros = true;
			}
			
		}
		
		return tieneNumeros;
	}
	
}

