package modulo6;

import java.util.Calendar;
import java.util.Date;

public class DateUtil {
	
	public DateUtil() {
		
	}
	
	public static int getAnio(Date pFecha) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(pFecha);
		
		
		
		return cal.get(Calendar.YEAR);

	}
	
	public static int getMes(Date pFecha) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(pFecha);
		
		
		
		return cal.get(Calendar.MONTH);

	}
	
	public static int getDia(Date pFecha) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(pFecha);
		
		
		
		return cal.get(Calendar.DAY_OF_MONTH);

	}
	
	public static boolean isFinDeSemana(Date pFecha) {
		boolean finDeSemana = false;
		Calendar cal = Calendar.getInstance();
		cal.setTime(pFecha);
		
		int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
		
		if (dayOfWeek == cal.SATURDAY || dayOfWeek == cal.SUNDAY) {
			
			finDeSemana = true;
		}
		
		return finDeSemana;

	}
	
	public static boolean isDiaDeSemana(Date pFecha) {
		boolean diaDeSemana = false;
		Calendar cal = Calendar.getInstance();
		cal.setTime(pFecha);
		
		int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
		
		if (dayOfWeek != cal.SATURDAY || dayOfWeek != cal.SUNDAY) {
			
			diaDeSemana = true;
		}
		
		return diaDeSemana;

	}
	
	public static int getDiaDeSemana(Date pFecha) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(pFecha);
		
		
		
		return cal.get(Calendar.DAY_OF_WEEK);

	}
	
	public static Calendar dateToCalendar(final Date date) {
		
		final Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar;
	}

}
