package modulostring;


public class Ejercicio4 {

	public static void main(String[] args) {
		
		String cadena = "esto es una prueba de clase string";
		System.out.println(cadena);
		int contVocales = 0;
		int contConsonantes = 0;
		
		cadena = cadena.replace(" ", "");
		for (int i = 0; i < cadena.length(); i++) {
			if (cadena.charAt(i) == 'a'|| cadena.charAt(i) == 'e'|| cadena.charAt(i) == 'i'|| cadena.charAt(i) == 'o'|| cadena.charAt(i) == 'u') {
				
				contVocales++;
			} else {
				
				contConsonantes++;
			}
		}
		System.out.println("Hay " + contVocales + " vocales");
		System.out.println("Hay " + contConsonantes + " consonantes" );

	}

}
