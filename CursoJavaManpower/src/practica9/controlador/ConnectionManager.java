package practica9.controlador;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManager {
	private static Connection conection;
	
	
	public static Connection conectar() throws ClassNotFoundException, SQLException {
		
		Class.forName("com.mysql.cj.jdbc.Driver");
		conection = DriverManager.getConnection("jdbc:mysql://localhost:8081/manpower", "root", "root");
		return conection;
	}
	
	public static void desConectar() throws SQLException{
		conection.close();
	}

}