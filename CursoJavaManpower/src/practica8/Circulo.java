package practica8;


public class Circulo extends Figura {

	private float radio;
	
	public Circulo() {	
		this.radio = 4;
		if (calcularArea() > Figura.maximaSuperficie) {
			Figura.maximaSuperficie = calcularArea();
		}
	}

	public Circulo(String nombre, float pRadio) {
		super(nombre);
		this.radio = pRadio;
		if (calcularArea() > Figura.maximaSuperficie) {
			Figura.maximaSuperficie = calcularArea();
		}
	}
	
	public String getValores() {
		
		return "";
	}
	
	public float calcularPerimetro() {
		
		float perimetro = 2 * (float)Math.PI * radio;
		return perimetro;
	}
	
	public float calcularArea() {
		
		float area = (float) ((float)Math.PI * Math.pow(radio, 2));
		return area;
	}	
	

	public float getRadio() {
		
		return radio;
	}
	public void setRadio(float radio) {
		
		this.radio = radio;
	}

	public int hashCode() {
		return super.hashCode() + (int)this.radio;
	}

	public boolean equals(Object obj) {
		
		return obj instanceof Circulo &&
				super.equals(obj) &&
				((Circulo) obj).getRadio() == radio;
	}

	public String toString() {
		return ", radio=" + radio;
	}
	

}
