package practica8;



public class Rectangulo extends Figura {

	private float lado1;
	private float lado2;
		
	public Rectangulo() {
		this.lado1 = 1;
		this.lado2 = 2;
		if (calcularArea() > Figura.maximaSuperficie) {
			
			Figura.maximaSuperficie = calcularArea();
		}
	}

	public Rectangulo(String nombre, float pLado1, float pLado2) {
		super(nombre);
		this.lado1 = pLado1;
		this.lado2 = pLado2;
		if (calcularArea() > Figura.maximaSuperficie) {
			
			Figura.maximaSuperficie = calcularArea();
		}
	}
	
	public float calcularPerimetro() {
		
		float perimetro = (lado1*2) + (lado2*2);
		return perimetro;
	}
	
	public float calcularArea() {
		
		float area = lado1 * lado2;
		return area;
	}
	
	public String getValores() {
		return "";
	}

	public float getLado1() {
		
		return lado1;
	}
	public void setLado1(float lado1) {
		
		this.lado1 = lado1;
	}

	public float getLado2() {
		
		return lado2;
	}
	public void setLado2(float lado2) {
		
		this.lado2 = lado2;
	}
	
	
	public int hashCode() {
		return super.hashCode() + (int)this.lado1 + (int)this.lado2;
	}

	public boolean equals(Object obj) {
		
		return 	obj instanceof Rectangulo &&
				super.equals(obj) &&
				((Rectangulo) obj).getLado1() == lado1 &&
				((Rectangulo) obj).getLado2() == lado2;
	}

	public String toString() {
		
		return ", lado1=" + lado1 + ", lado2=" + lado2;
	}
	
	

}