package practica8;


public class Cuadrado extends Figura {

	private float lado;
	
	public Cuadrado() {
		
		this.lado = 3;
		if (calcularArea() > Figura.maximaSuperficie) {
			Figura.maximaSuperficie = calcularArea();
		}
	}

	public Cuadrado(String nombre, float pLado) {
		super(nombre);
		this.lado = pLado;
		if (calcularArea() > Figura.maximaSuperficie) {
			Figura.maximaSuperficie = calcularArea();
		}
	}
	
	
	public float calcularPerimetro() {
		
		float perimetro = 4 * lado;
		return perimetro;
	}
	
	public float calcularArea() {
		
		float area = lado * lado;
		return area;
	}
	
	public String getValores() {
		return "";
	}

	public float getLado() {
		
		return lado;
	}
	public void setLado(float lado) {
		
		this.lado = lado;
	}	
	
	
	
	public int hashCode() {
		return super.hashCode() + (int)this.lado;
	}

	public boolean equals(Object obj) {
		
		return 	obj instanceof Cuadrado &&
				super.equals(obj) &&
				((Cuadrado) obj).getLado() == lado;
	}

	public String toString() {
		return ", lado=" + lado;
	}
	
	

}