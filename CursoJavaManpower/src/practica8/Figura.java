package practica8;

public abstract class Figura {
	
	protected static float maximaSuperficie;
	private String nombre;

		
	public Figura() {
		
	}
	
	public Figura(String nombre) {
		super();
		this.nombre = nombre;
	}

	public abstract String getValores();
	public abstract float calcularPerimetro();
	public abstract float calcularArea();
	
	
	public String getNombre() {
		
		return nombre;
	}
	public void setNombre(String nombre) {
		
		this.nombre = nombre;
	}

	public static float getMaximaSuperficie() {
		
		return maximaSuperficie;
	}
	public static void setMaximaSuperficie(float maximaSuperficie) {
		
		Figura.maximaSuperficie = maximaSuperficie;
	}


	public int hashCode() {
		return this.nombre.hashCode();
	}

	public boolean equals(Object obj) {
		
		return obj instanceof Figura &&
				this.nombre.equals(((Figura) obj).getNombre());
	}

	public String toString() {
		
		return "\nFigura: nombre=" + nombre + ", maximaSuperficie=" + maximaSuperficie;
	}
	
	
	
	

}