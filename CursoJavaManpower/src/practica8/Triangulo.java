package practica8;


public class Triangulo extends Figura {

	private float lado1;
	private float lado2;
	private float lado3;
	
	
	public Triangulo() {
		
		this.lado1 = 4;
		this.lado2 = 1;
		this.lado3 = 2;
		if (calcularArea() > Figura.maximaSuperficie) {
			Figura.maximaSuperficie = calcularArea();
		}
	}

	public Triangulo(String nombre, float pLado1, float pLado2, float pLado3) {
		super(nombre);
		this.lado1 = pLado1;
		this.lado2 = pLado2;
		this.lado3 = pLado3;
		if (calcularArea() > Figura.maximaSuperficie) {
			Figura.maximaSuperficie = calcularArea();
		}
	}
	
	public String getValores() {
		
		return "";
	}
	
	public float calcularPerimetro() {
		
		float per = lado1 + lado2 + lado3;
		return per;
	}
	
	public float calcularArea() {
		
		float sem = (lado1 + lado2 + lado3) / 2;
		float area = (float)Math.sqrt(sem * (sem - lado1) * (sem - lado2) * (sem - lado3));
		return area;
	}

	public float getLado1() {
		
		return lado1;
	}
	public void setLado1(float lado1) { 
	
		this.lado1 = lado1;
	}

	public float getLado2() {
		
		return lado2;
	}
	public void setLado2(float lado2) {
		
		this.lado2 = lado2;
	}

	public float getLado3() {
		
		return lado3;
	}
	public void setLado3(float lado3) {
		
		this.lado3 = lado3;
	}
	
	
	public int hashCode() {
		return super.hashCode() + (int)lado1 + (int)lado2 + (int)lado3;
	}

	public boolean equals(Object obj) {
		
		return 	obj instanceof Triangulo &&
				super.equals(obj) &&
				((Triangulo) obj).getLado1() == lado1 &&
				((Triangulo) obj).getLado2() == lado2 &&
				((Triangulo) obj).getLado3() == lado3;
	}

	public String toString() {
		
		return ", lado1=" + lado1 + ", lado2=" + lado2 + ", lado=" + lado3;
	}
	
	

}
