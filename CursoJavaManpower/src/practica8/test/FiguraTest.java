package practica8.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import practica8.Circulo;
import practica8.Cuadrado;
import practica8.Rectangulo;
import practica8.Triangulo;

class FiguraTest {
	
	Circulo circuloVacio, circuloLleno;
	Rectangulo rectanguloVacio, rectanguloLleno;
	Cuadrado cuadradoVacio, cuadradoLleno;
	Triangulo trianguloVacio, trianguloLleno;
	
	List<Cuadrado> listCuadrado;
	List<Circulo> listCirculo;
	List<Rectangulo> listRectangulo;
	List<Triangulo> listTriangulo;
	
	Set<Cuadrado> setCuadrado;
	Set<Circulo> setCirculo;
	Set<Rectangulo> setRectangulo;
	Set<Triangulo> setTriangulo;
//No sé que pasa con los hashSet
	@BeforeEach
	void setUp() throws Exception {
		
		circuloVacio = new Circulo();
		rectanguloVacio = new Rectangulo();
		cuadradoVacio = new Cuadrado();
		trianguloVacio = new Triangulo();
		
		circuloLleno = new Circulo("circuloLleno", 8);
		rectanguloLleno = new Rectangulo("rectanguloLleno", 6, 4);
		cuadradoLleno = new Cuadrado("cuadradoLleno", 2);
		trianguloLleno = new Triangulo("trianguloLleno", 4, 7, 5);
		
		listCuadrado = new ArrayList<Cuadrado>();
		listCuadrado.add(new Cuadrado("cuadrado", 5));
		
		listCirculo	= new ArrayList<Circulo>();
		listCirculo.add(new Circulo("circulo", 5));
		
		listRectangulo = new ArrayList<Rectangulo>();
		listRectangulo.add(new Rectangulo("rectangulo", 5, 6));
		
		listTriangulo = new ArrayList<Triangulo>();
		listTriangulo.add(new Triangulo("triangulo", 5, 6, 7));
		
		/*setCuadrado	= new HashSet<Cuadrado>();
		setCuadrado.add(cuadrado);
		setCuadrado.add(cuadradoLleno);
		
		setCirculo	= new HashSet<Circulo>();
		setCirculo.add(circulo);
		setCirculo.add(circuloLleno);
		
		setRectangulo = new HashSet<Rectangulo>();
		setRectangulo.add(rectangulo);
		setRectangulo.add(rectanguloLleno);
		
		setTriangulo = new HashSet<Triangulo>();
		setTriangulo.add(triangulo);
		setTriangulo.add(trianguloLleno);*/
		
	}

	@AfterEach
	void tearDown() throws Exception {
		
		circuloVacio = null;
		rectanguloVacio = null;
		cuadradoVacio = null;
		trianguloVacio = null;
	}

	@Test
	void testFigura() {
		assertEquals(4, circuloVacio.getRadio());
		assertEquals(1, rectanguloVacio.getLado1());
		assertEquals(3, cuadradoVacio.getLado());
		assertEquals(4, trianguloVacio.getLado1());	
	}
	
	@Test
	void equalsListaTrueCuadrado() {
		Cuadrado cuadrado = new Cuadrado("cuadrado", 5);
		assertTrue(listCuadrado.contains(cuadrado));
	}
	
	@Test
	void equalsListaTrueCirculo() {
		Circulo circulo = new Circulo("circulo", 5);
		assertTrue(listCirculo.contains(circulo));
	}
	
	@Test
	void equalsListaTrueRectangulo() {
		Rectangulo rectangulo = new Rectangulo("rectangulo", 5, 6);
		assertTrue(listRectangulo.contains(rectangulo));
	}
	
	@Test
	void equalsListaTrueTriangulo() {
		Triangulo triangulo = new Triangulo("triangulo", 5, 6, 7);
		assertTrue(listTriangulo.contains(triangulo));
	}
	
	
	@Test
	void equalsListaFalseCuadrado() {
		Cuadrado cuadrado = new Cuadrado("cuadrado", 10);
		assertFalse(listCuadrado.contains(cuadrado));
	}
	
	@Test
	void equalsListaFalseCirculo() {
		Circulo circulo = new Circulo("circulo", 10);
		assertFalse(listCirculo.contains(circulo));
	}
	
	@Test
	void equalsListaFalseRectangulo() {
		Rectangulo rectangulo = new Rectangulo("rectangulo", 10, 10);
		assertFalse(listRectangulo.contains(rectangulo));
	}
	
	@Test
	void equalsListaFalseTriangulo() {
		Triangulo triangulo = new Triangulo("triangulo", 10, 10, 10);
		assertFalse(listTriangulo.contains(triangulo));
	}
	
	@Test
	void listAddCuadrado() {
		Cuadrado cuadrado = new Cuadrado("cuadrado", 12);
		listCuadrado.add(cuadrado);
		assertTrue(listCuadrado.contains(cuadrado));	
	}
	
	@Test
	void listAddCirculo() {
		Circulo circulo = new Circulo("circulo", 12);
		listCirculo.add(circulo);
		assertTrue(listCirculo.contains(circulo));	
	}
	
	@Test
	void listAddRectangulo() {
		Rectangulo rectangulo = new Rectangulo("rectangulo", 12, 10);
		listRectangulo.add(rectangulo);
		assertTrue(listRectangulo.contains(rectangulo));	
	}
	
	@Test
	void listAddTriangulo() {
		Triangulo triangulo = new Triangulo("triangulo", 12, 10, 8);
		listTriangulo.add(triangulo);
		assertTrue(listTriangulo.contains(triangulo));	
	}
	
	/*@Test
	void testEqualsObjectTRUE() {
		Cuadrado cu = new Cuadrado();
		Circulo ci = new Circulo();
		Triangulo tr = new Triangulo();
		Rectangulo re = new Rectangulo();
		assertTrue(cuadradoVacio.equals(cu)); 
		assertTrue(circuloVacio.equals(ci)); 
		assertTrue(trianguloVacio.equals(tr)); 
		assertTrue(rectanguloVacio.equals(re)); 
	}
	
	@Test
	void testEqualsObjectFALSE() {
		Cuadrado cuadrado = new Cuadrado("cuadrado", 2);
		Circulo circulo = new Circulo("circulo", 2);
		Triangulo triangulo = new Triangulo("triangulo", 2, 2, 2);
		Rectangulo rectangulo = new Rectangulo("rectangulo", 2, 3);
		assertFalse(cuadradoVacio.equals(cuadrado)); 
		assertFalse(circuloVacio.equals(circulo)); 
		assertFalse(trianguloVacio.equals(triangulo)); 
		assertFalse(rectanguloVacio.equals(rectangulo));
	}*/
	@Test
	public void testPerimetroCuadrado(){
		Cuadrado cuadrado = new Cuadrado("Cuadrado" , 5f);
		float per = cuadrado.calcularPerimetro();
		assertEquals(per, 20f);
	}
	
	@Test
	public void testPerimetroCirculo(){
		Circulo circulo = new Circulo("Circulo",3f);
		float per = circulo.calcularPerimetro();
		assertEquals(per, 18,84f);
	}
	
	@Test
	public void testPerimetroRectangulo(){
		Rectangulo rectangulo = new Rectangulo("Rectángulo",18f,12f);
		float per = rectangulo.calcularPerimetro();
		assertEquals(per, 60f);
	}
	
	@Test
	public void testPerimetroTriangulo(){
		Triangulo triangulo = new Triangulo("Triangulo", 5f, 5f, 5f);
		float per = triangulo.calcularPerimetro();
		assertEquals(per, 15f);
	}
	
	@Test
	public void testAreaCuadrado(){
		Cuadrado cuadrado = new Cuadrado("Cuadrado" , 3f);
		float area = cuadrado.calcularArea();
		assertEquals(area, 9f);
	}
	
	@Test
	public void testAreaCirculo(){
		Circulo circulo = new Circulo("Circulo",3f);
		float area = circulo.calcularArea();
		assertEquals(area,28,26f);
	}
	
	@Test
	public void testAreaRectángulo(){
		Rectangulo rectangulo = new Rectangulo("Rectángulo",3f,2f);
		float area = rectangulo.calcularArea();
		assertEquals(area, 6f);
	}
	
	@Test
	public void testAreaTriángulo(){
		Triangulo triangulo = new Triangulo("Triangulo", 3f, 3f, 3f);
		float area = triangulo.calcularArea();
		assertEquals(area, 4,5f);
	}


}
