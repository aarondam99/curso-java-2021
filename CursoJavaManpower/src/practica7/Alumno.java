package practica7;



public class Alumno extends Persona {

	private int legajo;
	
	// Constructors
	public Alumno() {
		super("Aar�n", "Sanchez");
		this.legajo = 123456789;
	}
	public Alumno(int pLegajo) {
		super("Aaron", "Sanchez");
		this.legajo = pLegajo;
	}
	public Alumno(String pNombre, String pApellido, int pLegajo) {
		super(pNombre, pApellido);
		
		this.legajo = pLegajo;
	}

	public int getLegajo() {
		
		return legajo;
		
	}
	public void setLegajo(int legajo) {this.legajo = legajo;}
	
	
	public boolean equals(Object obj) {
		
		return obj instanceof Alumno &&
				super.equals(obj) &&
				this.legajo == ((Alumno) obj).getLegajo();
	}
	
	public int hashCode() {
		
		return super.hashCode() + this.legajo;
	}
	public String toString() {
		
		return "\nAlumno=" + super.toString() + "Legajo=" + this.legajo;
	}
	
	

}