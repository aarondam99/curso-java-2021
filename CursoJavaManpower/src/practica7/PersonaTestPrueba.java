package practica7;

public class PersonaTestPrueba {

	public static void main(String[] args) {
		
		Persona alumno = new Alumno("Sanchez", "Pepe", 1000);
		Persona profesor = new Profesor();
		
		System.out.println("alumno:" + alumno);
		System.out.println("profesor:" + profesor);
		//probando que el contructor funciona
		
		System.out.println("se creo el objeto 1:" + alumno.getNombre());
		System.out.println("su apellido es " + alumno.getApellido());
	
		System.out.println("\nse creo el objeto 2=" + profesor.getNombre());
		System.out.println("su apellido es " + profesor.getApellido());
		
		

	}

}
