package practica7;


public class Persona {

	private String apellido;
	private String nombre;
	
	// Constructors
	public Persona() {
		nombre = "Aaron";
		apellido = "Sanchez";
	}
	public Persona(String pNombre, String pApellido) {
		nombre = pNombre;
		apellido = pApellido;
	}
	
	public String getApellido() {
		
		return apellido;
	}
	public void setApellido(String pApellido) {
		
		this.apellido = pApellido;
	}
	
	public String getNombre() {
		
		return nombre;
	}
	public void setNombre(String pNombre) {
		
		this.nombre = pNombre;
	}
	
	
	public boolean equals(Object obj) {
		
		return obj instanceof Persona &&
				this.nombre.equals(((Persona) obj).getNombre())  &&
				this.apellido.equals(((Persona) obj).getApellido());
		
	}
	public int hashCode() {
		
		return this.nombre.hashCode() + this.apellido.hashCode();
	}
	public String toString() {
		
		return "Apellido=" + this.apellido + ", Nombre=" + this.nombre;
	}
	
	
	
	
}