package practica7.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import practica7.Alumno;
import practica7.Persona;
import practica7.Profesor;

class PersonaTest {

	Persona alumnoVacio, alumnoLleno, profesorVacio, profesorLleno;
	
	List<Persona> listPersona;
	Set<Persona> setPersona;
	
	@BeforeEach
	void setUp() throws Exception {
		
		alumnoVacio = new Alumno();
		alumnoLleno = new Alumno("Jose", "Martinez", 546567);
		profesorVacio = new Profesor();
		profesorLleno = new Profesor("Jose", "Martinez", "fdsfdvt");
		
		listPersona = new ArrayList<Persona>();
		listPersona.add(new Alumno());
		listPersona.add(new Alumno("Jose", "Martinez", 546567));
		
		setPersona = new HashSet<Persona>();
		setPersona.add(new Alumno());
		setPersona.add(new Alumno("Jose", "Martinez", 546567));
		
	}

	@AfterEach
	void tearDown() throws Exception {
		
		alumnoVacio = null;
		alumnoLleno = null;
		profesorVacio = null;
		profesorLleno = null;
		listPersona = null;
		setPersona = null;
	}
	
	@Test
	void equalsListaTRUE() {
		
		Persona alumno = new Alumno();
		assertTrue(listPersona.contains(alumno));	
	}
	
	@Test
	void equalsListaFALSE() {
		
		Persona alumno = new Alumno("Antonio", "Gomez", 546567);
		assertFalse(listPersona.contains(alumno));	
	}	
	
	@Test
	void listAdd() {
		
		Persona alumno = new Alumno("Jose", "Martinez", 546567);
		listPersona.add(alumno);
		assertTrue(listPersona.contains(alumno));	
	}
	
	@Test
	void setAdd() {
		
		Persona alumno = new Alumno();
		assertFalse(setPersona.add(alumno));	
	}

	@Test
	void testEqualsObjectTRUE() {
		
		Persona alumno = new Alumno();
		Persona profesor = new Profesor();
		assertTrue(alumnoVacio.equals(alumno)); 
		assertTrue(profesorVacio.equals(profesor)); 
	}
	
	@Test
	void testEqualsObjectFLASE() {
		
		Persona alumno = new Alumno("Antonio", "Martinez", 354648);
		Persona profesor = new Profesor("Antonio", "Martinez", "dsvdfghd");
		assertFalse(alumnoVacio.equals(alumno)); 
		assertFalse(profesorVacio.equals(profesor)); 
	}
	
	

}
