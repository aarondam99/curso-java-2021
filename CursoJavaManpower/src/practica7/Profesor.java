package practica7;



public class Profesor extends Persona {

	private String iosfa;
	
	// Constructors
	public Profesor() {
		super("Aaron", "Sanchez");
		
		this.iosfa = "ewfgefgv";
	}
	public Profesor(String pIosfa) {
		super("Aaron", "Sanchez");
		
		this.iosfa = pIosfa;
	}
	public Profesor(String pNombre, String pApellido, String pIosfa) {
		super(pNombre, pApellido);
		
		this.iosfa = pIosfa;
	}
	
	public String getIosfa() {
		
		return iosfa;
	}
	public void setIosfa(String iosfa) {
		
		this.iosfa = iosfa;
	}
	
	
	public boolean equals(Object obj) {
		
		return obj instanceof Profesor &&
				super.equals(obj) &&
				this.iosfa.equals(((Profesor) obj).getIosfa());	
	}
	
	public int hashCode() {
		
		return super.hashCode() + this.iosfa.hashCode();
	}
	
	public String toString() {
		
		return "\nProfesor=" + super.toString() + ", Iosfa=" + this.iosfa;
	}
	
	
	

}