package es.com.manpower.notas.util;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.junit.After;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ConnectionManagerTest {

	Connection con;

	@BeforeEach
	public void setUp() throws Exception {
		
		Class.forName("com.mysql.cj.jdbc.Driver");
		
		con = DriverManager.getConnection("jdbc:mysql://localhost:3306/manpower", "root", "Anasa-1987");
		
		ConnectionManager.conectar();
	}
	
	@After
	public void tearDown() throws SQLException {
		
		con = null;
		ConnectionManager.desConectar();
	}
	@Test
	void testConectar() {
		try {
			ConnectionManager.conectar();
			assertFalse(ConnectionManager.getConection().isClosed());
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			assertTrue(false);
			e.printStackTrace();
		}
		
	}

	@Test
	void testDesConectar() {
		try {
			ConnectionManager.desConectar();
			assertTrue(ConnectionManager.getConection().isClosed());
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}

	@Test
	void testGetConection() throws Exception {
		assertFalse(ConnectionManager.getConection().isClosed());
	}

}
