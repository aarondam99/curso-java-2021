package modulo3;

import java.util.Scanner;

public class Ejercicio21 {
	
public static void main(String[] args) {
		

		System.out.println("Introduce la categor�a(a, b, c)");
		Scanner sc = new Scanner(System.in);
		char categoria = sc.next().charAt(0);
		
		System.out.println("Introduce la antiguedad en a�os");
		int antiguedad = sc.nextInt();
		
		System.out.println("Introduce el sueldo");
		double sueldo = sc.nextDouble();
		
		if (antiguedad<=5 && antiguedad>=1) {
		
			sueldo = sueldo + sueldo*0.05;
			
		} else if (antiguedad<=10 && antiguedad>5) {
			
			sueldo = sueldo + sueldo*0.10;
			
		} else if (antiguedad>10) {
			
			sueldo = sueldo + sueldo*0.30;
		}
		
		switch (categoria) {
		case 'a': {
			
			sueldo = sueldo + 1000;
			
			break;
		}
		
		case 'b': {
			
			sueldo = sueldo + 2000;
			
			break;
		}

		case 'c': {
	
			sueldo = sueldo + 3000;
			
			break;
		}
		default:
			System.out.println("Categor�a no v�lida");
		}
		
		System.out.println("El sueldo neto del empleado es: " + sueldo + " euros");
		
		sc.close();
	}

}
