package modulo3;

import java.util.Scanner;

public class Ejercicio4 {

	public static void main(String[] args) {
		
		String a = "a";
		String b = "b";
		String c = "c";
		
		System.out.println("Introduce a, b o c");
		Scanner sc = new Scanner(System.in);
		String opcion = sc.nextLine();
		
		if (opcion.equals(a)) {
			
			System.out.println("Hijo");
		} else if (opcion.equals(b)) {
			
			System.out.println("Padres");
			
		} else if (opcion.equals(c)) {
			
			System.out.println("Abuelos");
			
		} else {
			
			System.out.println("Opci�n no v�lida");
		}
		
		sc.close();
	}

}
