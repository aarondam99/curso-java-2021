package modulo3;

import java.util.Scanner;

public class Ejercicio17b {

	public static void main(String[] args) {
		
		System.out.println("Introduce un n�mero");
		Scanner sc = new Scanner(System.in);
		int numero = sc.nextInt();
		int suma = 0;
		
		for (int i = 0; i <= 10; i+=2) {
			
			suma = suma + (numero*i);
				
				
			System.out.println(numero +" x " + i + " = " + numero*i);
		}
		
		System.out.println("La suma de los valores pares es " + suma);
		
		sc.close();
	}

}
