package modulo3;

import java.util.Scanner;

public class Ejercicio6 {

	public static void main(String[] args) {

		System.out.println("Introduce el n�mero");
		Scanner sc = new Scanner(System.in);
		int numero = sc.nextInt();
		
		if (numero==0) {
			
			System.out.println("Jardin de infantes");
			
		} else if (numero>0 && numero <=6) {
			
			System.out.println("Primaria");
			
		} else {
			
			System.out.println("N�mero no v�lido");
		}
		
		sc.close();
	}

}
