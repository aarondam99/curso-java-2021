package modulo3;

import java.util.Scanner;

public class Ejercicio5 {

	public static void main(String[] args) {
		
		System.out.println("Introduce tu puesto");
		Scanner sc = new Scanner(System.in);
		int puesto = sc.nextInt();
		
		switch (puesto) {
		case 1: {
			System.out.println("Medalla de oro");
			break;
		}
		
		case 2: {
			System.out.println("Medalla de plata");
			break;
		}

		case 3: {
			System.out.println("Medalla de bronce");
			break;
}
		default:
			System.out.println("Sigue participando");
			break;
		}
		
		sc.close();

	}

}
