package modulo3;

public class Ejercicio19 {
	
	public static void main(String[] args) {
		int contador = 0;
		int suma = 0;
		int promedio = 0;
		while (contador < 10) {
			contador++;
			int numero = (int)(Math.random()*10+1);
			suma = suma + numero;
			System.out.println(numero);
		}
		
		promedio = suma/10;
		System.out.println("La suma de los n�meros es " + suma);
		System.out.println("El promedio es " + promedio);
		//Yo prefiero hacerlo con un for, he elegido que el random sea hasta el n�mero 10
		/*for (int i = 0; i < 10; i++) {
			
			int numero = (int)(Math.random()*10+1);
			System.out.println(numero);
			
		}*/

	}

}
