package modulo3;

import java.util.Scanner;

public class Ejercicio11 {

	public static void main(String[] args) {
		
		System.out.println("Introduce la letra");
		Scanner sc = new Scanner(System.in);
		char letra = sc.next().charAt(0);
		
		if (letra == 'a' ||letra == 'e' ||letra == 'i' ||letra == 'o' ||letra == 'u') {
			
			System.out.println("La letra es vocal");
			
		} else {
			
			System.out.println("La letra es consonante");
		}
		
		sc.close();

	}

}
