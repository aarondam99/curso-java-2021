package modulo3;

import java.util.Scanner;

public class Ejercicio3 {

	public static void main(String[] args) {
		
		System.out.println("Introduce el n�mero del mes");
		Scanner sc = new Scanner(System.in);
		int mes = 0;
		int mes31 = 31;
		int mes30 = 30;
		int mes28 = 28;
		mes = sc.nextInt();
		
		
		switch (mes) {
		case 1: {
			
			System.out.println("Enero: " + mes31 );
			break;
		}
		
		case 2: {
			
			System.out.println("Febrero: " + mes28 );
			break;
		}

		case 3: {
			
			System.out.println("Marzo: " + mes31 );
			break;
		}

		case 4: {
			
			System.out.println("Abril: " + mes30 );
			break;
		}

		case 5: {
			
			System.out.println("Mayo: " + mes31 );
			break;
		}

		case 6: {
			
			System.out.println("Junio: " + mes30 );
			break;
		}

		case 7: {
			
			System.out.println("Julio: " + mes31 );
			break;
		}

		case 8: {
			System.out.println("Agosto: " + mes31 );
			break;
		}
		
		case 9: {
			System.out.println("Septiembre: " + mes30 );
			break;
		}
		
		case 10: {
			System.out.println("Octubre: " + mes31 );
			break;
		}
		
		case 11: {
			System.out.println("Noviembre: " + mes31 );
			break;
		}
		
		case 12: {
			System.out.println("Diciembre: " + mes31 );
			break;
		}
		default:
			System.out.println("Mes introducido no v�lido");
		}
		
		sc.close();
	}

}
