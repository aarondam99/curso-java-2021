package modulo3;

import java.util.Scanner;

public class Ejercicio15 {

	public static void main(String[] args) {
		
		System.out.println("Introduce la letra");
		Scanner sc = new Scanner(System.in);
		char letra = sc.next().charAt(0);
		
		switch (letra) {
		case 'a': {
			System.out.println("El veh�culo tiene 4 ruedas y un motor");
			break;
		}
		case 'b': {
			System.out.println("El veh�culo tiene 4 ruedas,  un motor, cierre centralizado y aire");
			break;
		}
		case 'c': {
			System.out.println("El veh�culo tiene 4 ruedas,  un motor, cierre centralizado, aire, airbag");
			break;
		}
		
		default:
			System.out.println("No se ha encontrado el veh�culo");;
		}
		sc.close();
	}

}
