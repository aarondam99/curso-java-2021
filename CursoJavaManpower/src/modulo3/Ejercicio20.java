package modulo3;


public class Ejercicio20 {

	public static void main(String[] args) {
		int contador = 0;
		int mayor = 0;
		int menor = 10;

		do {
			
			int numero = (int)(Math.random()*10+1);
			if (mayor<numero) {
				mayor = numero;
			}
				
			if (numero<menor) {
				menor = numero;
			}
			contador++;
			System.out.println(numero);
		
			
		} while (contador < 10);
		
		System.out.println("El mayor n�mero es " + mayor);
		System.out.println("El menor n�mero es " + menor);
	}

}
