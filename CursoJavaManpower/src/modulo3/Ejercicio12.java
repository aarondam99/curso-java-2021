package modulo3;

import java.util.Scanner;

public class Ejercicio12 {

	public static void main(String[] args) {

		System.out.println("Introduce el n�mero");
		Scanner sc = new Scanner(System.in);
		int numero = sc.nextInt();
		
		if (numero>0 && numero<=12) {
			
			System.out.println("El n�mero pertenece a la primera docena");
			
		} else if (numero>12 && numero <=24) {
			
			System.out.println("El n�mero pertenece a la segunda docena");
			
		} else if (numero>24 && numero <=36) {
			
			System.out.println("El n�mero pertenece a la segunda docena");
			
		} else {
			
			System.out.println("El n�mero est� fuera de rango");
		}
		
		sc.close();
	}

}
