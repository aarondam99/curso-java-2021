package modulo3;

import java.util.Scanner;

public class Ejercicio16 {

	public static void main(String[] args) {

		System.out.println("Introduce un n�mero");
		Scanner sc = new Scanner(System.in);
		int numero = sc.nextInt();
		
		for (int i = 1; i <= 10; i++) {
			System.out.println(numero +" x " + i + " = " + numero*i);
		}

		sc.close();
	}

}
