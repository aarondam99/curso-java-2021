package patrones.factory;

public class Tijera extends PiedraPapelTijeraFactory {

	public Tijera() {
		
		numero = 2;
		nombre = "tijera";
	}

	@Override
	public boolean isMe(int pNumero) {
		
		return pNumero ==2;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory pPpt) {
		
		int result = 0;
		
		switch (pPpt.numero) {
		
		case 0: {
			
			result = -1;
			
			break;
		}
		
		case 1: {
			
			result = 1;
			
			break;
		}

		case 2: {

			result = 0;

			break;
		}
		default:
			break;
		}
		
		
		return result;
	}

}
