package patrones.factory.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import patrones.factory.Papel;
import patrones.factory.Piedra;
import patrones.factory.PiedraPapelTijeraFactory;
import patrones.factory.Tijera;

class PiedraPapelTijeraFactoryTest {
	
	PiedraPapelTijeraFactory piedra;
	PiedraPapelTijeraFactory papel;
	PiedraPapelTijeraFactory tijera;
	@BeforeEach
	void setUp() throws Exception {
		
		piedra = new Piedra();
		papel = new Papel();
		tijera = new Tijera();
	}

	@AfterEach
	void tearDown() throws Exception {
		
		piedra = null;
		papel = null;
		tijera = null;
	}
	
	@Test
	public void testGetInstancePiedra() {
		
		assertTrue(PiedraPapelTijeraFactory.getInstance(0) instanceof Piedra);
	}
	
	@Test
	public void testGetInstancePapel() {
		
		assertTrue(PiedraPapelTijeraFactory.getInstance(1) instanceof Papel);
	}

	
	@Test
	public void testGetInstanceTijera() {
		
		assertTrue(PiedraPapelTijeraFactory.getInstance(2) instanceof Tijera);
	}


	@Test
	public void testComparaPiedraPierdaPapel() {
		assertEquals(-1, piedra.comparar(papel));
	}
	
	@Test
	public void testComparaPiedraGanaPapel() {
		assertEquals(1, papel.comparar(piedra));
	}

}
