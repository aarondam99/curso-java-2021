package patrones.factory;

public class Papel extends PiedraPapelTijeraFactory {

	public Papel() {
		
		numero = 1;
		nombre = "papel";

	}

	@Override
	public boolean isMe(int pNumero) {
		// TODO Auto-generated method stub
		return pNumero == 1;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory pPpt) {
		int result = 0;
		
		switch (pPpt.numero) {
		
		case 0: {
			
			result = 1;
			
			break;
		}
		
		case 1: {
			
			result = 0;
			
			break;
		}

		case 2: {

			result = -1;

			break;
		}
		default:
			break;
		}
		
		
		return result;
	
	}

}
