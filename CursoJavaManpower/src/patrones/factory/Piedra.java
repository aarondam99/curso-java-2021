package patrones.factory;

public class Piedra extends PiedraPapelTijeraFactory {

	public Piedra() {
		
		numero = 0;
		nombre = "piedra";
		
		
		
		
		
	}
		@Override
		public boolean isMe(int pNumero) {
			return pNumero == 0;
	}
		@Override
		public int comparar(PiedraPapelTijeraFactory pPpt) {
			
			int result = 0;
			
			switch (pPpt.numero) {
			
			case 0: {
				
				result = 0;
				descripcionResultado = "empatamos";
				break;
			}
			
			case 1: {
				
				result = -1;
				descripcionResultado = "perdimos";
				break;
			}

			case 2: {
	
				result = 1;
				descripcionResultado = "ganamos";
				break;
			}
			default:
				break;
			}
			
			
			return result;
		}

}
