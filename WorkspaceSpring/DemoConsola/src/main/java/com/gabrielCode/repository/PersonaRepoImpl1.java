package com.gabrielCode.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import com.gabrielCode.DemoConsolaApplication;
import com.gabrielCode.repository.IPersona;

@Repository
@Qualifier("persona1")
public class PersonaRepoImpl1 implements IPersona {
	
	private static Logger log = LoggerFactory.getLogger(DemoConsolaApplication.class);

	@Override
	public void registrar(String pNombre) {
		// TODO Auto-generated method stub
		log.info("Se registró FELIZMENTE...a:" + pNombre);
	}
	
	

}